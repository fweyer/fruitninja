﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Score Elements")]
    public int score;
    public Text scoreText;
    public int highScore;
    public Text highScoreText;

    [Header("GameOver Elements")]
    public GameObject gameOverPanel;

    private void Awake()
    {
        gameOverPanel.SetActive(false);
        GetHighscore();
    }


    private void GetHighscore()
    {
        highScore = PlayerPrefs.GetInt("Highscore");
        highScoreText.text = "Best: " + highScore;
    }

    public void IncreaseScore(int num)
    {
        score += num;
        scoreText.text = score.ToString();

        if (score > highScore)
        {
            PlayerPrefs.SetInt("Highscore", score);
            highScoreText.text = "Best: " + score.ToString();
        }

    }

    public void OnBombHit()
    {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
        Debug.Log("Bomb hit");
    }

    public void RestartGame()
    {
        score = 0;
        scoreText.text = "0";

        gameOverPanel.SetActive(false);

        foreach(GameObject g  in GameObject.FindGameObjectsWithTag("Interactable"))
        {
            Destroy(g);
        }
        GetHighscore();
        Time.timeScale = 1;
    }
}
