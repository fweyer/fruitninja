﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{

    public float minVelocity = 0.1f;

    // Wir brauchen den Rigidbody der Blade
    private Rigidbody2D rb;
    private Vector3 lastMousePos;
    private Vector3 mousVelo;
    private Collider2D col;

    // In der Awake Methode setzen wir den Rigidbody Component unserer Blade. 
    // Awake wird sehr früh im Lebenszyklus aufgrufen, noch früher als "Start"
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        col.enabled = isMouseMoving();
        SetBladeToMouse();
    }

    // Mit dieser Methode setzen wir die Position unserer Blade and die Position unserer Maus.
    private void SetBladeToMouse()
    {
        var mousePos = Input.mousePosition;
        // Wir setzen die Z Position unserer Mouse auf 10, damit wir auf der gleichen
        // Z Ebene sind, wie unsere Früchte, nämlich auf z= 0. Denn unsere Kamera ist auf z=-10
        // und wir wollen ja mit unserer Blade die Früchte zerstören.
        mousePos.z = 10;
        rb.position = Camera.main.ScreenToWorldPoint(mousePos);
    }

    private bool isMouseMoving()
    {
        Vector3 curMousePos = transform.position;
        float traveld = (lastMousePos - curMousePos).magnitude;
        lastMousePos = curMousePos;
        
        if(traveld > minVelocity)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
