﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Hiermit erstellen wir eine Blade objekt, wenn das Objekt mit dem wir kollidieren das 
        // Blade Script hat.
        Blade b = collision.GetComponent<Blade>();

        // Wenn es nämlich kein Script hat, ist das Objekt "b" leer.
        // In dem fall "returnen" wir, also wir springen aus diesem Methodenaufruf raus.
        if (!b)
        {
            return;
        }

        FindObjectOfType<GameManager>().OnBombHit();

    }
}
